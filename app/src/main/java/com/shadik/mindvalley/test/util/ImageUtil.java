package com.shadik.mindvalley.test.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import khan.shadik.networkinglibrary.common.Priority;
import khan.shadik.networkinglibrary.entry.NetworkingBase;
import khan.shadik.networkinglibrary.error.LibError;
import khan.shadik.networkinglibrary.interfaces.AnalyticsListener;
import khan.shadik.networkinglibrary.interfaces.BitmapRequestListener;
import khan.shadik.networkinglibrary.internal.ImageLoader;

import com.shadik.mindvalley.test.R;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Shadik on 7/22/2016.
 */
public class ImageUtil {

    public static void loadImage(String url, ImageView img) {
        ImageLoader.getInstance().get(url, ImageLoader.getImageListener(img,
                R.drawable.ic_launcher, R.drawable.ic_launcher));
        //LoadImage.with(img, url);
    }

    public static void loadImageDirect(final Activity context, final String url, final ImageView img, final String TAG) {
        NetworkingBase.get(url)
                .setTag("imageRequestTag")
                .setPriority(Priority.MEDIUM)
                .setImageScaleType(null)
                .setBitmapMaxHeight(0)
                .setBitmapMaxWidth(0)
                .setBitmapConfig(Bitmap.Config.ARGB_8888)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d(TAG, " bytesSent : " + bytesSent);
                        Log.d(TAG, " bytesReceived : " + bytesReceived);
                        Log.d(TAG, " isFromCache : " + isFromCache);
                    }
                })
                .getAsBitmap(new BitmapRequestListener() {
                    @Override
                    public void onResponse(final Bitmap response) {
                        Log.d(TAG, "onResponse Bitmap");
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img.setImageBitmap(response);
                            }
                        });

                    }

                    @Override
                    public void onError(LibError error) {
                        if (error.getErrorCode() != 0) {

                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }
}
