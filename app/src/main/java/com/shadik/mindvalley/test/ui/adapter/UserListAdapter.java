package com.shadik.mindvalley.test.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shadik.mindvalley.test.R;
import com.shadik.mindvalley.test.model.UserDetailedModel;
import com.shadik.mindvalley.test.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

import khan.shadik.networkinglibrary.widget.NetworkImageView;

/**
 * Created by Shadik on 7/22/2016.
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<UserDetailedModel> userList;

    public UserListAdapter(Context mContext, ArrayList<UserDetailedModel> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView username;
        protected NetworkImageView profileImage;
        protected NetworkImageView backgroundImage;
        public CardView card_view;

        public ViewHolder(View v) {
            super(v);
            username = (TextView) v.findViewById(R.id.tvUserName);
            profileImage = (NetworkImageView) v.findViewById(R.id.imgProfile);
            backgroundImage = (NetworkImageView) v.findViewById(R.id.imgBackground);
            card_view = (CardView) v.findViewById(R.id.card_view);
        }
    }

    @Override
    public int getItemCount() {
        return userList != null ? userList.size() : 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.username.setText(Html.fromHtml(userList.get(position).getUser().getName()));
        holder.profileImage.setImageUrl(userList.get(position).getUser().getProfileImage().getLarge());
        holder.backgroundImage.setImageUrl(userList.get(position).getUrls().getFull());
        //ImageUtil.loadImageDirect((Activity) mContext, userList.get(position).getUser().getProfileImage().getLarge(), holder.profileImage, mContext.getClass().getName());
        //ImageUtil.loadImage(userList.get(position).getUser().getProfileImage().getLarge(), holder.profileImage);
        //ImageUtil.loadImageDirect((Activity) mContext, userList.get(position).getUrls().getFull(), holder.backgroundImage, mContext.getClass().getName());
        //ImageUtil.loadImage(userList.get(position).getUrls().getFull(), holder.backgroundImage);
        //holder.card_view.setOnClickListener(new CustomClickListener(holder, position, shopList.get(position)));
    }

    public void addAll(List<UserDetailedModel> list) {
        userList.addAll(list);
        notifyDataSetChanged();
    }

}
