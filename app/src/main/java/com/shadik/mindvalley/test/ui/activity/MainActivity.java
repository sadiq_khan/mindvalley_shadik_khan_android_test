package com.shadik.mindvalley.test.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shadik.mindvalley.test.R;
import com.shadik.mindvalley.test.common.ApiEndPoint;
import com.shadik.mindvalley.test.model.UserDetailedModel;
import com.shadik.mindvalley.test.ui.adapter.UserListAdapter;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import khan.shadik.networkinglibrary.common.Priority;
import khan.shadik.networkinglibrary.entry.NetworkingBase;
import khan.shadik.networkinglibrary.error.LibError;
import khan.shadik.networkinglibrary.interfaces.AnalyticsListener;
import khan.shadik.networkinglibrary.interfaces.JSONArrayRequestListener;

public class MainActivity extends AppCompatActivity {
    private static String TAG = MainActivity.class.getName();
   /* private ImageView imageViewDirect;
    private ImageView imageViewApi;
    private TextView textVewName;*/

    private RecyclerView userListRecycler;
    private LinearLayoutManager layoutManager;
    private UserListAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Users");
        setSupportActionBar(toolbar);

        iniTUi();
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Cancel All the requests", Snackbar.LENGTH_LONG)
                        .setAction("Yes", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Canceling All the request
                                cancelAllRequest();
                            }
                        }).show();
            }
        });

        //Load image
        loadImage();
        loadImageDirect();
        //Load Json Array Data
        textVewName = (TextView) findViewById(R.id.textVewName);
        loadUsers();*/
    }

    private void iniTUi() {
        userListRecycler = (RecyclerView) findViewById(R.id.rvUserList);
        layoutManager = new LinearLayoutManager(this);
        userListRecycler.setLayoutManager(layoutManager);
        userListRecycler.setItemAnimator(new DefaultItemAnimator());
        userAdapter = new UserListAdapter(this, new ArrayList<UserDetailedModel>());
        //userAdapter.setOnRecycleViewClickListener(this);
        userListRecycler.setAdapter(userAdapter);
        loadUsers();
    }

    private void loadUsers() {
        NetworkingBase.get(ApiEndPoint.BASE_URL)
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .setAnalyticsListener(new AnalyticsListener() {
                    @Override
                    public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                        Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                        Log.d(TAG, " bytesSent : " + bytesSent);
                        Log.d(TAG, " bytesReceived : " + bytesReceived);
                        Log.d(TAG, " isFromCache : " + isFromCache);
                    }
                })
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "onResponse array : " + response.toString());
                        Type collectionType = new TypeToken<List<UserDetailedModel>>() {
                        }.getType();
                        List<UserDetailedModel> data = new Gson().fromJson(response.toString(), collectionType);
                        updateUi(data);
                    }

                    @Override
                    public void onError(LibError error) {
                        if (error.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    private void updateUi(final List<UserDetailedModel> response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response != null || response.size() > 0) {
                    userAdapter.addAll(response);
                    userAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void cancelAllRequest() {
        NetworkingBase.cancel(this);
    }


}
