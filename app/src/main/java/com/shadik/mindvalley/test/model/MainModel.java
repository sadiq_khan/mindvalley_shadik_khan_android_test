package com.shadik.mindvalley.test.model;

import java.util.List;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class MainModel {
    private List<UserDetailedModel> data;

    public List<UserDetailedModel> getData() {
        return data;
    }

    public void setData(List<UserDetailedModel> data) {
        this.data = data;
    }
}
