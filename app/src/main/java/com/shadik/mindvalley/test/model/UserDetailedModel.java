package com.shadik.mindvalley.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class UserDetailedModel {

    private String id;
    @SerializedName("created_at")
    private String createdAt;
    private int width;
    private int height;
    private String color;
    private int likes;
    @SerializedName("liked_by_user")
    private boolean likedByUser;
    private UserModel user;
    private UrlModel urls;
    private LinkModel links;
    @SerializedName("current_user_collections")
    private List<?> currentUserCollections;
    private List<CategoryModel> categories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public UrlModel getUrls() {
        return urls;
    }

    public void setUrls(UrlModel urls) {
        this.urls = urls;
    }

    public LinkModel getLinks() {
        return links;
    }

    public void setLinks(LinkModel links) {
        this.links = links;
    }

    public List<?> getCurrentUserCollections() {
        return currentUserCollections;
    }

    public void setCurrentUserCollections(List<?> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    public List<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryModel> categories) {
        this.categories = categories;
    }


}
