package com.shadik.mindvalley.test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class CategoryModel {

    private int id;
    private String title;
    @SerializedName("photo_count")
    private int photoCount;

    private CategoryLinkModel links;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public CategoryLinkModel getLinks() {
        return links;
    }

    public void setLinks(CategoryLinkModel links) {
        this.links = links;
    }


}
