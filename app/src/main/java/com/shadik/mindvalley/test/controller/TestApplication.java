package com.shadik.mindvalley.test.controller;

import android.app.Application;
import android.util.Log;

import khan.shadik.networkinglibrary.entry.NetworkingBase;
import khan.shadik.networkinglibrary.common.ConnectionQuality;
import khan.shadik.networkinglibrary.interfaces.ConnectionQualityChangeListener;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class TestApplication extends Application {

    private static final String TAG = TestApplication.class.getSimpleName();
    private static TestApplication appInstance = null;

    public static TestApplication getInstance() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;

        NetworkingBase.initialize(getApplicationContext());
        NetworkingBase.enableLogging();
        NetworkingBase.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
            @Override
            public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
                Log.d(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality +
                        " currentBandwidth : " + currentBandwidth);
            }
        });

    }
}
