package com.shadik.mindvalley.test.model;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class LinkModel {

    private String self;
    private String html;
    private String download;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }
}
