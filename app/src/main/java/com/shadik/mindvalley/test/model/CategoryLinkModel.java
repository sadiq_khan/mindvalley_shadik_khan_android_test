package com.shadik.mindvalley.test.model;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class CategoryLinkModel {

    private String self;
    private String photos;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
