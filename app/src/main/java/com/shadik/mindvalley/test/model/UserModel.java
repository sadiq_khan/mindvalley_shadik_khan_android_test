package com.shadik.mindvalley.test.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class UserModel {

    private String id;
    private String username;
    private String name;
    @SerializedName("profile_image")
    private UserProfileModel profileImage;
    private UserLinkModel links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserProfileModel getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(UserProfileModel profileImage) {
        this.profileImage = profileImage;
    }

    public UserLinkModel getLinks() {
        return links;
    }

    public void setLinks(UserLinkModel links) {
        this.links = links;
    }


}
