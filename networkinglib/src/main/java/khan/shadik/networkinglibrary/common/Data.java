package khan.shadik.networkinglibrary.common;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okio.Source;

/**
 * Created by Shadik on 7/21/2016.
 */
public class Data {

    public int code;

    public Headers headers;

    public long length;

    public Source source;

    public HttpUrl url;
}
