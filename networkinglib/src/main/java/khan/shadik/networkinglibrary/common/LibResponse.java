package khan.shadik.networkinglibrary.common;

import khan.shadik.networkinglibrary.error.LibError;

/**
 * Created by Shadik on 7/21/2016.
 */
public class LibResponse<T> {

    private final T mResult;

    private final LibError mError;

    public static <T> LibResponse<T> success(T result) {
        return new LibResponse<>(result);
    }

    public static <T> LibResponse<T> failed(LibError error) {
        return new LibResponse<>(error);
    }

    private LibResponse(T result) {
        this.mResult = result;
        this.mError = null;
    }

    private LibResponse(LibError error) {
        this.mResult = null;
        this.mError = error;
        this.mError.setErrorCode(0);
        this.mError.setErrorDetail(Constants.PARSE_ERROR);
    }

    public T getResult() {
        return mResult;
    }

    public boolean isSuccess() {
        return mError == null;
    }

    public LibError getError() {
        return mError;
    }
}
