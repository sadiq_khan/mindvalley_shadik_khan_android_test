package khan.shadik.networkinglibrary.common;

import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface RequestBuilder {
    RequestBuilder setPriority(Priority priority);

    RequestBuilder setTag(Object tag);

    RequestBuilder addHeaders(String key, String value);

    RequestBuilder addHeaders(HashMap<String, String> headerMap);

    RequestBuilder addQueryParameter(String key, String value);

    RequestBuilder addQueryParameter(HashMap<String, String> queryParameterMap);

    RequestBuilder addPathParameter(String key, String value);

    RequestBuilder doNotCacheResponse();

    RequestBuilder getResponseOnlyIfCached();

    RequestBuilder getResponseOnlyFromNetwork();

    RequestBuilder setMaxAgeCacheControl(int maxAge, TimeUnit timeUnit);

    RequestBuilder setMaxStaleCacheControl(int maxStale, TimeUnit timeUnit);

    RequestBuilder setExecutor(Executor executor);

    RequestBuilder setOkHttpClient(OkHttpClient okHttpClient);

    RequestBuilder setUserAgent(String userAgent);

}
