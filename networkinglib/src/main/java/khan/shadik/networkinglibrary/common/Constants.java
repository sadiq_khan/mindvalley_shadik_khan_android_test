package khan.shadik.networkinglibrary.common;

/**
 * Created by Shadik on 7/21/2016.
 */
public class Constants {

    public static final int MAX_CACHE_SIZE = 10 * 1024 * 1024;
    public static final int UPDATE = 0x01;
    public static final String CACHE_DIR_NAME = "cache_an";
    public static final String CONNECTION_ERROR = "connectionError";
    public static final String RESPONSE_FROM_SERVER_ERROR = "responseFromServerError";
    public static final String REQUEST_CANCELLED_ERROR = "requestCancelledError";
    public static final String PARSE_ERROR = "parseError";
    public static final String PREFETCH = "prefetch";
    public static final String ANDROID_NETWORKING = "networkinglib";
}
