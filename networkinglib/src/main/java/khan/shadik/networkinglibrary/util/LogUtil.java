package khan.shadik.networkinglibrary.util;

import android.util.Log;

import khan.shadik.networkinglibrary.common.Constants;

/**
 * Created by Shadik on 7/21/2016.
 */
public class LogUtil {

    private static boolean IS_LOGGING_ENABLED = false;
    private static String TAG = Constants.ANDROID_NETWORKING;

    private LogUtil() {

    }

    public static void enableLogging() {
        IS_LOGGING_ENABLED = true;
    }

    public static void disableLogging() {
        IS_LOGGING_ENABLED = false;
    }

    public static void setTag(String tag) {
        if (tag == null) {
            return;
        }
        TAG = tag;
    }

    public static void d(String message) {
        if (IS_LOGGING_ENABLED) {
            Log.d(TAG, message);
        }
    }

    public static void e(String message) {
        if (IS_LOGGING_ENABLED) {
            Log.e(TAG, message);
        }
    }

    public static void i(String message) {
        if (IS_LOGGING_ENABLED) {
            Log.i(TAG, message);
        }
    }

    public static void w(String message) {
        if (IS_LOGGING_ENABLED) {
            Log.w(TAG, message);
        }
    }

    public static void wtf(String message) {
        if (IS_LOGGING_ENABLED) {
            Log.wtf(TAG, message);
        }
    }
}
