package khan.shadik.networkinglibrary.core;

import java.util.concurrent.Executor;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface ExecutorSupplier {

    LibExecutor forNetworkTasks();

    LibExecutor forImmediateNetworkTasks();

    Executor forMainThreadTasks();
}
