package khan.shadik.networkinglibrary.entry;

import android.graphics.Bitmap;
import android.widget.ImageView;

import khan.shadik.networkinglibrary.error.LibError;
import khan.shadik.networkinglibrary.internal.ImageLoader;

/**
 * Created by skhan4 on 7/21/2016.
 */
public class LoadImage {

    public static void with(final ImageView imageView, String imageUrl) {
        ImageLoader.getInstance().get(imageUrl,
                new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {

                        if (response.getBitmap() != null) {
                            setImageBitmap(imageView, response.getBitmap());
                        } else {
                            setImageResource(imageView);
                        }
                    }

                    @Override
                    public void onError(LibError error) {
                        setImageResource(imageView);
                    }
                });
    }

    public static void setImageResource(ImageView imageView) {
        setImageBitmap(imageView, null);
    }

    public static void setImageResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    public static void setImageBitmap(ImageView img, Bitmap b) {
        img.setImageBitmap(b);
    }


}
