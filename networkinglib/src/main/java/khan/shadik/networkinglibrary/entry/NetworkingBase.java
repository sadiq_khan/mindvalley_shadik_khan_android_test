package khan.shadik.networkinglibrary.entry;

/**
 * Created by Shadik on 7/21/2016.
 */

import android.content.Context;

import khan.shadik.networkinglibrary.common.ConnectionClassManager;
import khan.shadik.networkinglibrary.common.ConnectionQuality;
import khan.shadik.networkinglibrary.common.Constants;
import khan.shadik.networkinglibrary.common.LibRequest;
import khan.shadik.networkinglibrary.core.Core;
import khan.shadik.networkinglibrary.interfaces.ConnectionQualityChangeListener;
import khan.shadik.networkinglibrary.internal.ImageLoader;
import khan.shadik.networkinglibrary.internal.InternalNetworking;
import khan.shadik.networkinglibrary.internal.LibRequestQueue;
import khan.shadik.networkinglibrary.util.LogUtil;
import khan.shadik.networkinglibrary.util.Utils;
import okhttp3.OkHttpClient;

/**
 * Entry point to this library
 */
public class NetworkingBase {

    /**
     * private constructor to prevent instantiation of this class
     */
    private NetworkingBase() {
    }

    /**
     * Initializes NetworkingBase with the default config.
     *
     * @param context The context
     */
    public static void initialize(Context context) {
        InternalNetworking.setClientWithCache(context.getApplicationContext());
        LibRequestQueue.initialize();
        ImageLoader.initialize();
    }

    /**
     * Initializes NetworkingBase with the specified config.
     *
     * @param context      The context
     * @param okHttpClient The okHttpClient
     */
    public static void initialize(Context context, OkHttpClient okHttpClient) {
        if (okHttpClient != null && okHttpClient.cache() == null) {
            okHttpClient = okHttpClient.newBuilder().cache(Utils.getCache(context.getApplicationContext(), Constants.MAX_CACHE_SIZE, Constants.CACHE_DIR_NAME)).build();
        }
        InternalNetworking.setClient(okHttpClient);
        LibRequestQueue.initialize();
        ImageLoader.initialize();
    }


    /**
     * Method to set connectionQualityChangeListener
     *
     * @param connectionChangeListener The connectionQualityChangeListener
     */
    public static void setConnectionQualityChangeListener(ConnectionQualityChangeListener connectionChangeListener) {
        ConnectionClassManager.getInstance().setListener(connectionChangeListener);
    }

    /**
     * Method to set connectionQualityChangeListener
     */
    public static void removeConnectionQualityChangeListener() {
        ConnectionClassManager.getInstance().removeListener();
    }

    /**
     * Method to make GET request
     *
     * @param url The url on which request is to be made
     * @return The GetRequestBuilder
     */
    public static LibRequest.GetRequestBuilder get(String url) {
        return new LibRequest.GetRequestBuilder(url);
    }

    /**
     * Method to make HEAD request
     *
     * @param url The url on which request is to be made
     * @return The HeadRequestBuilder
     */
    public static LibRequest.HeadRequestBuilder head(String url) {
        return new LibRequest.HeadRequestBuilder(url);
    }

    /**
     * Method to make POST request
     *
     * @param url The url on which request is to be made
     * @return The PostRequestBuilder
     */
    public static LibRequest.PostRequestBuilder post(String url) {
        return new LibRequest.PostRequestBuilder(url);
    }

    /**
     * Method to make PUT request
     *
     * @param url The url on which request is to be made
     * @return The PutRequestBuilder
     */
    public static LibRequest.PutRequestBuilder put(String url) {
        return new LibRequest.PutRequestBuilder(url);
    }

    /**
     * Method to make DELETE request
     *
     * @param url The url on which request is to be made
     * @return The DeleteRequestBuilder
     */
    public static LibRequest.DeleteRequestBuilder delete(String url) {
        return new LibRequest.DeleteRequestBuilder(url);
    }

    /**
     * Method to make PATCH request
     *
     * @param url The url on which request is to be made
     * @return The PatchRequestBuilder
     */
    public static LibRequest.PatchRequestBuilder patch(String url) {
        return new LibRequest.PatchRequestBuilder(url);
    }

    /**
     * Method to make download request
     *
     * @param url      The url on which request is to be made
     * @param dirPath  The directory path on which file is to be saved
     * @param fileName The file name with which file is to be saved
     * @return The DownloadBuilder
     */
    public static LibRequest.DownloadBuilder download(String url, String dirPath, String fileName) {
        return new LibRequest.DownloadBuilder(url, dirPath, fileName);
    }

    /**
     * Method to make upload request
     *
     * @param url The url on which request is to be made
     * @return The MultiPartBuilder
     */
    public static LibRequest.MultiPartBuilder upload(String url) {
        return new LibRequest.MultiPartBuilder(url);
    }

    /**
     * Method to cancel requests with the given tag
     *
     * @param tag The tag with which requests are to be cancelled
     */
    public static void cancel(Object tag) {
        LibRequestQueue.getInstance().cancelRequestWithGivenTag(tag, false);
    }

    /**
     * Method to force cancel requests with the given tag
     *
     * @param tag The tag with which requests are to be cancelled
     */
    public static void forceCancel(Object tag) {
        LibRequestQueue.getInstance().cancelRequestWithGivenTag(tag, true);
    }

    /**
     * Method to cancel all given request
     */
    public static void cancelAll() {
        LibRequestQueue.getInstance().cancelAll(false);
    }

    /**
     * Method to force cancel all given request
     */
    public static void forceCancelAll() {
        LibRequestQueue.getInstance().cancelAll(true);
    }

    /**
     * Method to enable logging
     */
    public static void enableLogging() {
        LogUtil.enableLogging();
    }

    /**
     * Method to enable logging with tag
     *
     * @param tag The tag for logging
     */
    public static void enableLogging(String tag) {
        LogUtil.enableLogging();
        LogUtil.setTag(tag);
    }

    /**
     * Method to disable logging
     */
    public static void disableLogging() {
        LogUtil.disableLogging();
    }

    /**
     * Method to evict a bitmap with given key from LruCache
     *
     * @param key The key of the bitmap
     */
    public static void evictBitmap(String key) {
        final ImageLoader.ImageCache imageCache = ImageLoader.getInstance().getImageCache();
        if (imageCache != null && key != null) {
            imageCache.evictBitmap(key);
        }
    }

    /**
     * Method to clear LruCache
     */
    public static void evictAllBitmap() {
        final ImageLoader.ImageCache imageCache = ImageLoader.getInstance().getImageCache();
        if (imageCache != null) {
            imageCache.evictAllBitmap();
        }
    }

    /**
     * Method to set userAgent globally
     *
     * @param userAgent The userAgent
     */
    public static void setUserAgent(String userAgent) {
        LibRequestQueue.getInstance().setUserAgent(userAgent);
    }

    /**
     * Method to get currentBandwidth
     *
     * @return currentBandwidth
     */
    public static int getCurrentBandwidth() {
        return ConnectionClassManager.getInstance().getCurrentBandwidth();
    }

    /**
     * Method to get currentConnectionQuality
     *
     * @return currentConnectionQuality
     */
    public static ConnectionQuality getCurrentConnectionQuality() {
        return ConnectionClassManager.getInstance().getCurrentConnectionQuality();
    }

    /**
     * Shuts NetworkingBase down
     */
    public static void shutDown() {
        Core.shutDown();
        evictAllBitmap();
        ConnectionClassManager.getInstance().removeListener();
        ConnectionClassManager.shutDown();
    }
}
