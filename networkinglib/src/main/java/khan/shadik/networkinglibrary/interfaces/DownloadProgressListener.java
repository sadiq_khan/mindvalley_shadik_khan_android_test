package khan.shadik.networkinglibrary.interfaces;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface DownloadProgressListener {

    void onProgress(long bytesDownloaded, long totalBytes);
}
