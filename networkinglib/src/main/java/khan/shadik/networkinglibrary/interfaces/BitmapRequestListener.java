package khan.shadik.networkinglibrary.interfaces;

import android.graphics.Bitmap;

import khan.shadik.networkinglibrary.error.LibError;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface BitmapRequestListener {

    void onResponse(Bitmap response);

    void onError(LibError ANError);
}
