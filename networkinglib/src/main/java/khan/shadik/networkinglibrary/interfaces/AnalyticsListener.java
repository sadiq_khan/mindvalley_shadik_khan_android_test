package khan.shadik.networkinglibrary.interfaces;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface AnalyticsListener {
    void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache);
}
