package khan.shadik.networkinglibrary.interfaces;

import org.json.JSONObject;

import khan.shadik.networkinglibrary.error.LibError;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface JSONObjectRequestListener {

    void onResponse(JSONObject response);

    void onError(LibError ANError);
}
