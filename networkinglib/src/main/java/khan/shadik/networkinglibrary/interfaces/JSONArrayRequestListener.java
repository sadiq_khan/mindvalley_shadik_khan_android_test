package khan.shadik.networkinglibrary.interfaces;

import org.json.JSONArray;

import khan.shadik.networkinglibrary.error.LibError;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface JSONArrayRequestListener {

    void onResponse(JSONArray response);

    void onError(LibError ANError);
}
