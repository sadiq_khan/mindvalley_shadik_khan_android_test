package khan.shadik.networkinglibrary.interfaces;

import khan.shadik.networkinglibrary.error.LibError;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface StringRequestListener {
    void onResponse(String response);

    void onError(LibError ANError);

}
