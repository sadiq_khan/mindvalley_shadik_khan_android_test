package khan.shadik.networkinglibrary.interfaces;

import khan.shadik.networkinglibrary.common.ConnectionQuality;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface ConnectionQualityChangeListener {
    void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth);
}
