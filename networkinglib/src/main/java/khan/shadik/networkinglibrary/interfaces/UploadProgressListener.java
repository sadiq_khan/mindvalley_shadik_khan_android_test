package khan.shadik.networkinglibrary.interfaces;

/**
 * Created by Shadik on 7/21/2016.
 */
public interface UploadProgressListener {

    void onProgress(long bytesUploaded, long totalBytes);
}
