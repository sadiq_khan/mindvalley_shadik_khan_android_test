package khan.shadik.networkinglibrary.internal;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import khan.shadik.networkinglibrary.common.LibRequest;
import khan.shadik.networkinglibrary.common.Priority;
import khan.shadik.networkinglibrary.core.Core;
import khan.shadik.networkinglibrary.util.LogUtil;

/**
 * Created by Shadik on 7/21/2016.
 */
public class LibRequestQueue {
    private final static String TAG = LibRequestQueue.class.getSimpleName();
    private final Set<LibRequest> mCurrentRequests = new HashSet<LibRequest>();
    private AtomicInteger mSequenceGenerator = new AtomicInteger();
    private static LibRequestQueue sInstance = null;
    private String userAgent = null;

    public static void initialize() {
        getInstance();
    }

    public static LibRequestQueue getInstance() {
        if (sInstance == null) {
            synchronized (LibRequestQueue.class) {
                sInstance = new LibRequestQueue();
            }
        }
        return sInstance;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public interface RequestFilter {
        boolean apply(LibRequest request);
    }


    private void cancel(RequestFilter filter, boolean forceCancel) {
        synchronized (mCurrentRequests) {
            try {
                for (Iterator<LibRequest> iterator = mCurrentRequests.iterator(); iterator.hasNext(); ) {
                    LibRequest request = iterator.next();
                    if (filter.apply(request)) {
                        request.cancel(forceCancel);
                        if (request.isCanceled()) {
                            request.destroy();
                            iterator.remove();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelAll(boolean forceCancel) {
        synchronized (mCurrentRequests) {
            try {
                for (Iterator<LibRequest> iterator = mCurrentRequests.iterator(); iterator.hasNext(); ) {
                    LibRequest request = iterator.next();
                    request.cancel(forceCancel);
                    if (request.isCanceled()) {
                        request.destroy();
                        iterator.remove();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelRequestWithGivenTag(final Object tag, final boolean forceCancel) {
        try {
            if (tag == null) {
                return;
            }
            cancel(new RequestFilter() {
                @Override
                public boolean apply(LibRequest request) {
                    return request.getTag() == tag;
                }
            }, forceCancel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSequenceNumber() {
        return mSequenceGenerator.incrementAndGet();
    }

    public LibRequest addRequest(LibRequest request) {
        synchronized (mCurrentRequests) {
            try {
                mCurrentRequests.add(request);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (userAgent != null && request.getUserAgent() == null) {
                request.setUserAgent(userAgent);
            }
            request.setSequenceNumber(getSequenceNumber());
            if (request.getPriority() == Priority.IMMEDIATE) {
                request.setFuture(Core.getInstance().getExecutorSupplier().forImmediateNetworkTasks().submit(new InternalRunnable(request)));
            } else {
                request.setFuture(Core.getInstance().getExecutorSupplier().forNetworkTasks().submit(new InternalRunnable(request)));
            }
            LogUtil.d("addRequest: after addition - mCurrentRequests size: " + mCurrentRequests.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return request;
    }

    public void finish(LibRequest request) {
        synchronized (mCurrentRequests) {
            try {
                mCurrentRequests.remove(request);
                LogUtil.d("finish: after removal - mCurrentRequests size: " + mCurrentRequests.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
