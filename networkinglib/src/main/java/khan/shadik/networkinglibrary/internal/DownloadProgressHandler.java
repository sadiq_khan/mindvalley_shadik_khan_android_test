/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package khan.shadik.networkinglibrary.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

import khan.shadik.networkinglibrary.common.Constants;
import khan.shadik.networkinglibrary.interfaces.DownloadProgressListener;
import khan.shadik.networkinglibrary.model.Progress;

/**
 * Created by Shadik on 7/21/2016.
 */
public class DownloadProgressHandler extends Handler {

    private final WeakReference<DownloadProgressListener> mDownloadProgressListenerWeakRef;

    public DownloadProgressHandler(DownloadProgressListener downloadProgressListener) {
        super(Looper.getMainLooper());
        mDownloadProgressListenerWeakRef = new WeakReference<>(downloadProgressListener);
    }

    @Override
    public void handleMessage(Message msg) {
        final DownloadProgressListener downloadProgressListener = mDownloadProgressListenerWeakRef.get();
        switch (msg.what) {
            case Constants.UPDATE:
                if (downloadProgressListener != null) {
                    final Progress progress = (Progress) msg.obj;
                    downloadProgressListener.onProgress(progress.currentBytes, progress.totalBytes);
                }
                break;
            default:
                super.handleMessage(msg);
                break;
        }
    }
}
