/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package khan.shadik.networkinglibrary.internal;

import java.io.IOException;

import khan.shadik.networkinglibrary.common.Constants;
import khan.shadik.networkinglibrary.common.Data;
import khan.shadik.networkinglibrary.common.LibRequest;
import khan.shadik.networkinglibrary.common.LibResponse;
import khan.shadik.networkinglibrary.common.Priority;
import khan.shadik.networkinglibrary.core.Core;
import khan.shadik.networkinglibrary.error.LibError;
import khan.shadik.networkinglibrary.util.LogUtil;

import static khan.shadik.networkinglibrary.common.RequestType.DOWNLOAD;
import static khan.shadik.networkinglibrary.common.RequestType.MULTIPART;
import static khan.shadik.networkinglibrary.common.RequestType.SIMPLE;

/**
 * Created by amitshekhar on 22/03/16.
 */
public class InternalRunnable implements Runnable {

    private static final String TAG = InternalRunnable.class.getSimpleName();
    private final Priority priority;
    public final int sequence;
    public final LibRequest request;

    public InternalRunnable(LibRequest request) {
        this.request = request;
        this.sequence = request.getSequenceNumber();
        this.priority = request.getPriority();
    }

    @Override
    public void run() {
        LogUtil.d("execution started : " + request.toString());
        switch (request.getRequestType()) {
            case SIMPLE:
                goForSimpleRequest();
                break;
            case DOWNLOAD:
                goForDownloadRequest();
                break;
            case MULTIPART:
                goForUploadRequest();
                break;
        }
        LogUtil.d("execution done : " + request.toString());
    }

    private void goForSimpleRequest() {
        Data data = null;
        try {
            data = InternalNetworking.performSimpleRequest(request);
            if (data.code == 304) {
                request.finish();
                return;
            }
            if (data.code >= 400) {
                LibError LibError = new LibError(data);
                LibError = request.parseNetworkError(LibError);
                LibError.setErrorCode(data.code);
                LibError.setErrorDetail(Constants.RESPONSE_FROM_SERVER_ERROR);
                deliverError(request, LibError);
                return;
            }

            LibResponse response = request.parseResponse(data);
            if (!response.isSuccess()) {
                deliverError(request, response.getError());
                return;
            }
            request.deliverResponse(response);
        } catch (LibError se) {
            se = request.parseNetworkError(se);
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);
        } catch (Exception e) {
            LibError se = new LibError(e);
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);

        } finally {
            if (data != null && data.source != null) {
                try {
                    data.source.close();
                } catch (IOException ignored) {
                    LogUtil.d("Unable to close source data");
                }
            }
        }
    }

    private void goForDownloadRequest() {
        Data data = null;
        try {
            data = InternalNetworking.performDownloadRequest(request);
            if (data.code >= 400) {
                LibError LibError = new LibError();
                LibError = request.parseNetworkError(LibError);
                LibError.setErrorCode(data.code);
                LibError.setErrorDetail(Constants.RESPONSE_FROM_SERVER_ERROR);
                deliverError(request, LibError);
                return;
            }
            request.updateDownloadCompletion();
        } catch (LibError se) {
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);
        } catch (Exception e) {
            LibError se = new LibError(e);
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);
        }
    }

    private void goForUploadRequest() {
        Data data = null;
        try {
            data = InternalNetworking.performUploadRequest(request);
            if (data.code == 304) {
                request.finish();
                return;
            }
            if (data.code >= 400) {
                LibError LibError = new LibError(data);
                LibError = request.parseNetworkError(LibError);
                LibError.setErrorCode(data.code);
                LibError.setErrorDetail(Constants.RESPONSE_FROM_SERVER_ERROR);
                deliverError(request, LibError);
                return;
            }
            LibResponse response = request.parseResponse(data);
            if (!response.isSuccess()) {
                deliverError(request, response.getError());
                return;
            }
            request.deliverResponse(response);
        } catch (LibError se) {
            se = request.parseNetworkError(se);
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);
        } catch (Exception e) {
            LibError se = new LibError(e);
            se.setErrorDetail(Constants.CONNECTION_ERROR);
            se.setErrorCode(0);
            deliverError(request, se);
        } finally {
            if (data != null && data.source != null) {
                try {
                    data.source.close();
                } catch (IOException ignored) {
                    LogUtil.d("Unable to close source data");
                }
            }
        }
    }

    public Priority getPriority() {
        return priority;
    }

    private void deliverError(final LibRequest request, final LibError LibError) {
        Core.getInstance().getExecutorSupplier().forMainThreadTasks().execute(new Runnable() {
            public void run() {
                request.deliverError(LibError);
                request.finish();
            }
        });
    }
}
