package khan.shadik.networkinglibrary.internal;

import android.content.Context;
import android.net.TrafficStats;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import khan.shadik.networkinglibrary.common.ConnectionClassManager;
import khan.shadik.networkinglibrary.common.Constants;
import khan.shadik.networkinglibrary.common.Data;
import khan.shadik.networkinglibrary.common.LibRequest;
import khan.shadik.networkinglibrary.core.Core;
import khan.shadik.networkinglibrary.error.LibError;
import khan.shadik.networkinglibrary.interfaces.AnalyticsListener;
import khan.shadik.networkinglibrary.util.Utils;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static khan.shadik.networkinglibrary.common.Method.DELETE;
import static khan.shadik.networkinglibrary.common.Method.GET;
import static khan.shadik.networkinglibrary.common.Method.HEAD;
import static khan.shadik.networkinglibrary.common.Method.PATCH;
import static khan.shadik.networkinglibrary.common.Method.POST;
import static khan.shadik.networkinglibrary.common.Method.PUT;

/**
 * Created by Shadik on 7/21/2016.
 */

public class InternalNetworking {
    private static final String USER_AGENT = "User-Agent";

    private static OkHttpClient sHttpClient = getClient();

    public static Data performSimpleRequest(LibRequest request) throws LibError {
        Data data = new Data();
        Request okHttpRequest = null;
        try {
            Request.Builder builder = new Request.Builder().url(request.getUrl());
            if (request.getUserAgent() != null) {
                builder.addHeader(USER_AGENT, request.getUserAgent());
            }
            Headers requestHeaders = request.getHeaders();
            if (requestHeaders != null) {
                builder.headers(requestHeaders);
                if (request.getUserAgent() != null && !requestHeaders.names().contains(USER_AGENT)) {
                    builder.addHeader(USER_AGENT, request.getUserAgent());
                }
            }
            RequestBody requestBody = null;
            switch (request.getMethod()) {
                case GET: {
                    builder = builder.get();
                    break;
                }
                case POST: {
                    requestBody = request.getRequestBody();
                    builder = builder.post(requestBody);
                    break;
                }
                case PUT: {
                    requestBody = request.getRequestBody();
                    builder = builder.put(requestBody);
                    break;
                }
                case DELETE: {
                    requestBody = request.getRequestBody();
                    builder = builder.delete(requestBody);
                    break;
                }
                case HEAD: {
                    builder = builder.head();
                    break;
                }
                case PATCH: {
                    requestBody = request.getRequestBody();
                    builder = builder.patch(requestBody);
                    break;
                }
            }
            if (request.getCacheControl() != null) {
                builder.cacheControl(request.getCacheControl());
            }
            okHttpRequest = builder.build();

            if (request.getOkHttpClient() != null) {
                request.setCall(request.getOkHttpClient().newBuilder().cache(sHttpClient.cache()).build().newCall(okHttpRequest));
            } else {
                request.setCall(sHttpClient.newCall(okHttpRequest));
            }
            final long startTime = System.currentTimeMillis();
            final long startBytes = TrafficStats.getTotalRxBytes();
            Response okResponse = request.getCall().execute();
            data.url = okResponse.request().url();
            data.code = okResponse.code();
            data.headers = okResponse.headers();
            data.source = okResponse.body().source();
            data.length = okResponse.body().contentLength();
            final long timeTaken = System.currentTimeMillis() - startTime;
            if (okResponse.cacheResponse() == null) {
                final long finalBytes = TrafficStats.getTotalRxBytes();
                final long diffBytes;
                if (startBytes == TrafficStats.UNSUPPORTED || finalBytes == TrafficStats.UNSUPPORTED) {
                    diffBytes = data.length;
                } else {
                    diffBytes = finalBytes - startBytes;
                }
                ConnectionClassManager.getInstance().updateBandwidth(diffBytes, timeTaken);
                sendAnalytics(request.getAnalyticsListener(), timeTaken, (requestBody != null && requestBody.contentLength() != 0) ? requestBody.contentLength() : -1, data.length, false);
            } else if (request.getAnalyticsListener() != null) {
                if (okResponse.networkResponse() == null) {
                    sendAnalytics(request.getAnalyticsListener(), timeTaken, 0, 0, true);
                } else {
                    sendAnalytics(request.getAnalyticsListener(), timeTaken, (requestBody != null && requestBody.contentLength() != 0) ? requestBody.contentLength() : -1, 0, true);
                }
            }
        } catch (IOException ioe) {
            if (okHttpRequest != null) {
                data.url = okHttpRequest.url();
            }
            throw new LibError(data, ioe);
        }

        return data;
    }

    public static Data performDownloadRequest(final LibRequest request) throws LibError {
        Data data = new Data();
        Request okHttpRequest = null;
        try {
            Request.Builder builder = new Request.Builder().url(request.getUrl());
            if (request.getUserAgent() != null) {
                builder.addHeader(USER_AGENT, request.getUserAgent());
            }
            Headers requestHeaders = request.getHeaders();
            if (requestHeaders != null) {
                builder.headers(requestHeaders);
                if (request.getUserAgent() != null && !requestHeaders.names().contains(USER_AGENT)) {
                    builder.addHeader(USER_AGENT, request.getUserAgent());
                }
            }
            builder = builder.get();
            if (request.getCacheControl() != null) {
                builder.cacheControl(request.getCacheControl());
            }
            okHttpRequest = builder.build();

            OkHttpClient okHttpClient;

            if (request.getOkHttpClient() != null) {
                okHttpClient = request.getOkHttpClient().newBuilder().cache(sHttpClient.cache())
                        .addNetworkInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Response originalResponse = chain.proceed(chain.request());
                                return originalResponse.newBuilder()
                                        .body(new ResponseProgressBody(originalResponse.body(), request.getDownloadProgressListener()))
                                        .build();
                            }
                        }).build();
            } else {
                okHttpClient = sHttpClient.newBuilder()
                        .addNetworkInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Response originalResponse = chain.proceed(chain.request());
                                return originalResponse.newBuilder()
                                        .body(new ResponseProgressBody(originalResponse.body(), request.getDownloadProgressListener()))
                                        .build();
                            }
                        }).build();
            }
            request.setCall(okHttpClient.newCall(okHttpRequest));
            final long startTime = System.currentTimeMillis();
            final long startBytes = TrafficStats.getTotalRxBytes();
            Response okResponse = request.getCall().execute();
            data.url = okResponse.request().url();
            data.code = okResponse.code();
            data.headers = okResponse.headers();
            Utils.saveFile(okResponse, request.getDirPath(), request.getFileName());
            data.length = okResponse.body().contentLength();
            final long timeTaken = System.currentTimeMillis() - startTime;
            if (okResponse.cacheResponse() == null) {
                final long finalBytes = TrafficStats.getTotalRxBytes();
                final long diffBytes;
                if (startBytes == TrafficStats.UNSUPPORTED || finalBytes == TrafficStats.UNSUPPORTED) {
                    diffBytes = data.length;
                } else {
                    diffBytes = finalBytes - startBytes;
                }
                ConnectionClassManager.getInstance().updateBandwidth(diffBytes, timeTaken);
                sendAnalytics(request.getAnalyticsListener(), timeTaken, -1, data.length, false);
            } else if (request.getAnalyticsListener() != null) {
                sendAnalytics(request.getAnalyticsListener(), timeTaken, -1, 0, true);
            }
        } catch (IOException ioe) {
            if (okHttpRequest != null) {
                data.url = okHttpRequest.url();
            }
            try {
                File destinationFile = new File(request.getDirPath() + File.separator + request.getFileName());
                if (destinationFile.exists()) {
                    destinationFile.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            throw new LibError(data, ioe);
        }
        return data;
    }


    public static Data performUploadRequest(LibRequest request) throws LibError {
        Data data = new Data();
        Request okHttpRequest = null;
        try {
            Request.Builder builder = new Request.Builder().url(request.getUrl());
            if (request.getUserAgent() != null) {
                builder.addHeader(USER_AGENT, request.getUserAgent());
            }
            Headers requestHeaders = request.getHeaders();
            if (requestHeaders != null) {
                builder.headers(requestHeaders);
                if (request.getUserAgent() != null && !requestHeaders.names().contains(USER_AGENT)) {
                    builder.addHeader(USER_AGENT, request.getUserAgent());
                }
            }
            final RequestBody requestBody = request.getMultiPartRequestBody();
            final long requestBodyLength = requestBody.contentLength();
            builder = builder.post(new RequestProgressBody(requestBody, request.getUploadProgressListener()));
            if (request.getCacheControl() != null) {
                builder.cacheControl(request.getCacheControl());
            }
            okHttpRequest = builder.build();
            if (request.getOkHttpClient() != null) {
                request.setCall(request.getOkHttpClient().newBuilder().cache(sHttpClient.cache()).build().newCall(okHttpRequest));
            } else {
                request.setCall(sHttpClient.newCall(okHttpRequest));
            }
            final long startTime = System.currentTimeMillis();
            Response okResponse = request.getCall().execute();
            data.url = okResponse.request().url();
            data.code = okResponse.code();
            data.headers = okResponse.headers();
            data.source = okResponse.body().source();
            data.length = okResponse.body().contentLength();
            final long timeTaken = System.currentTimeMillis() - startTime;
            if (request.getAnalyticsListener() != null) {
                if (okResponse.cacheResponse() == null) {
                    sendAnalytics(request.getAnalyticsListener(), timeTaken, requestBodyLength, data.length, false);
                } else {
                    if (okResponse.networkResponse() == null) {
                        sendAnalytics(request.getAnalyticsListener(), timeTaken, 0, 0, true);
                    } else {
                        sendAnalytics(request.getAnalyticsListener(), timeTaken, requestBodyLength != 0 ? requestBodyLength : -1, 0, true);
                    }
                }
            }
        } catch (IOException ioe) {
            if (okHttpRequest != null) {
                data.url = okHttpRequest.url();
            }
            throw new LibError(data, ioe);
        }
        return data;
    }

    private static void sendAnalytics(final AnalyticsListener analyticsListener, final long timeTakenInMillis, final long bytesSent, final long bytesReceived, final boolean isFromCache) {
        Core.getInstance().getExecutorSupplier().forMainThreadTasks().execute(new Runnable() {
            @Override
            public void run() {
                if (analyticsListener != null) {
                    analyticsListener.onReceived(timeTakenInMillis, bytesSent, bytesReceived, isFromCache);
                }
            }
        });
    }

    public static OkHttpClient getClient() {
        if (sHttpClient == null) {
            return getDefaultClient();
        }
        return sHttpClient;
    }

    public static OkHttpClient getDefaultClient() {
        return new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public static void setClientWithCache(Context context) {
        sHttpClient = new OkHttpClient().newBuilder()
                .cache(Utils.getCache(context, Constants.MAX_CACHE_SIZE, Constants.CACHE_DIR_NAME))
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public static void setClient(OkHttpClient okHttpClient) {
        sHttpClient = okHttpClient;
    }

}
