package khan.shadik.networkinglibrary.error;

import khan.shadik.networkinglibrary.common.Constants;
import khan.shadik.networkinglibrary.common.Data;

/**
 * Created by Shadik on 7/21/2016.
 */
public class LibError extends Exception {

    private Data data;

    private String errorBody;

    private int errorCode = 0;

    private String errorDetail;

    public LibError() {
    }

    public LibError(Data data) {
        this.data = data;
    }

    public LibError(String message) {
        super(message);
    }

    public LibError(String message, Data data) {
        super(message);
        this.data = data;
    }

    public LibError(String message, Throwable throwable) {
        super(message, throwable);
    }

    public LibError(String message, Data data, Throwable throwable) {
        super(message, throwable);
        this.data = data;
    }

    public LibError(Data data, Throwable throwable) {
        super(throwable);
        this.data = data;
    }

    public LibError(Throwable throwable) {
        super(throwable);
    }

    public Data getData() {
        return data;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getErrorDetail() {
        return this.errorDetail;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setCancellationMessageInError() {
        this.errorDetail = Constants.REQUEST_CANCELLED_ERROR;
    }

    public String getErrorBody() {
        return errorBody;
    }

    public void setErrorBody(String errorBody) {
        this.errorBody = errorBody;
    }

}
